# draft_poc

Proof-of-concept code for Project Draft

## Running

```bash
# install ASDF (https://github.com/asdf-vm/asdf)
# unzip the data.zip from Slack into the data/ directory
$ asdf plugin-add python
$ asdf install
$ pipenv run go
```

This will output the mock rating data to stdout, and create an `empbook.csv`
file that can be imported back into HASTUS.

## Local Development

```bash
$ pipenv run format  # formats the Python files
$ pipenv run typecheck  # typechecks the code using Mypy
$ pipenv run lint  # uses Pylint to ensure that code meets our guidelines
$ pipenv run test  # unit tests
```
