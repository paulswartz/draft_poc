from datetime import timedelta
from draft_poc.pick import timedelta_as_timestr, timedelta_as_durationstr


def test_timedelta_as_timestr():
    assert timedelta_as_timestr(timedelta(hours=0, minutes=0)) == "1200a"
    assert timedelta_as_timestr(timedelta(hours=5, minutes=6)) == "506a"
    assert timedelta_as_timestr(timedelta(hours=12, minutes=13)) == "1213p"
    assert timedelta_as_timestr(timedelta(hours=24, minutes=26)) == "1226x"
    assert timedelta_as_timestr(timedelta(hours=25, minutes=59)) == "159x"


def test_timedelta_as_durationstr():
    assert timedelta_as_durationstr(timedelta(hours=0, minutes=0)) == "0h00"
    assert timedelta_as_durationstr(timedelta(hours=5, minutes=6)) == "5h06"
    assert timedelta_as_durationstr(timedelta(hours=12, minutes=13)) == "12h13"
