from datetime import date
from draft_poc.roster import (
    parse,
    CrewAssignment,
    DayOfWeek,
    NonCrewAssignment,
    PositionType,
    RosterDay,
    RosterPosition,
)


class TestRosterDay:
    def test_simple_parse(self):
        row = "BUS22021|Rsc122|546296506|1-BB|122001|3|Monday|1001|10008782|64748"
        expected = RosterDay(
            booking_id="BUS22021",
            roster_set_id="Rsc122",
            roster_set_internal_id=546296506,
            roster_id="1-BB",
            roster_position_id="122001",
            roster_position_internal_id=3,
            day=DayOfWeek.MONDAY,
            assignment=CrewAssignment("1001", 10008782),
            crew_schedule_id=64748,
        )
        actual = parse(row)
        assert expected == actual

    def test_more_complex(self):
        # day of the rating, off-list
        row = "BUS22021|Rsc122|546296506|1-BB|122002|4|05/31/2021|OL||65067"
        expected = RosterDay(
            booking_id="BUS22021",
            roster_set_id="Rsc122",
            roster_set_internal_id="546296506",
            roster_id="1-BB",
            roster_position_id="122002",
            roster_position_internal_id=4,
            day=date(2021, 5, 31),
            assignment=NonCrewAssignment.OFF_LIST,
            crew_schedule_id=65067,
        )
        actual = parse(row)
        assert expected == actual


class TestRosterPosition:
    @staticmethod
    def from_days(days):
        booking_id = "BUS22021"
        roster_set_id = "Rsc122"
        roster_set_internal_id = 12345
        roster_id = "1-BB"
        roster_position_id = "abcd"
        roster_position_internal_id = 3
        crew_schedule_id = 4567
        return RosterPosition.from_days(
            [
                RosterDay(
                    booking_id=booking_id,
                    roster_set_id=roster_set_id,
                    roster_set_internal_id=roster_set_internal_id,
                    roster_id=roster_id,
                    roster_position_id=roster_position_id,
                    roster_position_internal_id=roster_position_internal_id,
                    crew_schedule_id=crew_schedule_id,
                    day=day,
                    assignment=assignment,
                )
                for (day, assignment) in days
            ]
        )

    def test_multiple_assignments(self):
        crew_assignment1 = CrewAssignment("0001", 1)
        crew_assignment2 = CrewAssignment("2000", 2)
        days = [
            (DayOfWeek.MONDAY, crew_assignment1),
            (DayOfWeek.TUESDAY, crew_assignment1),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.OFF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.OFF),
            (DayOfWeek.FRIDAY, crew_assignment2),
            (DayOfWeek.SATURDAY, NonCrewAssignment.LIST_8HR),
            (DayOfWeek.SUNDAY, NonCrewAssignment.LIST_8HR),
            (date(2021, 5, 4), NonCrewAssignment.OFF_LIST),
            (date(2021, 3, 24), NonCrewAssignment.OFF_LIST),
        ]
        position = self.from_days(days)
        expected = [
            (DayOfWeek.SUNDAY, NonCrewAssignment.LIST_8HR),
            (DayOfWeek.MONDAY, CrewAssignment("0001", 1)),
            (DayOfWeek.TUESDAY, CrewAssignment("0001", 1)),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.OFF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.OFF),
            (DayOfWeek.FRIDAY, CrewAssignment("2000", 2)),
            (DayOfWeek.SATURDAY, NonCrewAssignment.LIST_8HR),
            (date(2021, 3, 24), NonCrewAssignment.OFF_LIST),
            (date(2021, 5, 4), NonCrewAssignment.OFF_LIST),
        ]
        actual = position.day_table()
        assert expected == actual

        assert position.type() == PositionType.LIST

    def test_all_same(self):
        days = [
            (DayOfWeek.MONDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.TUESDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.FRIDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.SATURDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.SUNDAY, NonCrewAssignment.VACATION_RELIEF),
            (date(2021, 3, 24), NonCrewAssignment.VACATION_RELIEF),
            (date(2021, 5, 4), NonCrewAssignment.VACATION_RELIEF),
        ]
        position = self.from_days(days)
        expected = [
            (DayOfWeek.SUNDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.MONDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.TUESDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.FRIDAY, NonCrewAssignment.VACATION_RELIEF),
            (DayOfWeek.SATURDAY, NonCrewAssignment.VACATION_RELIEF),
        ]
        actual = position.day_table()
        assert expected == actual

        assert position.type() == PositionType.VACATION_RELIEF

    def test_same_assignment(self):
        crew_assignment = CrewAssignment("0001", 1)
        days = [
            (DayOfWeek.MONDAY, crew_assignment),
            (DayOfWeek.TUESDAY, crew_assignment),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.OFF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.OFF),
            (DayOfWeek.FRIDAY, crew_assignment),
            (DayOfWeek.SATURDAY, NonCrewAssignment.OFF),
            (DayOfWeek.SUNDAY, crew_assignment),
            (date(2021, 5, 4), NonCrewAssignment.OFF),
            (date(2021, 3, 24), NonCrewAssignment.OFF),  # already off on Wednesday
        ]
        position = self.from_days(days)
        expected = [
            (DayOfWeek.SUNDAY, crew_assignment),
            (DayOfWeek.MONDAY, crew_assignment),
            (DayOfWeek.TUESDAY, crew_assignment),
            (DayOfWeek.WEDNESDAY, NonCrewAssignment.OFF),
            (DayOfWeek.THURSDAY, NonCrewAssignment.OFF),
            (DayOfWeek.FRIDAY, crew_assignment),
            (DayOfWeek.SATURDAY, NonCrewAssignment.OFF),
            (date(2021, 5, 4), NonCrewAssignment.OFF),
        ]
        actual = position.day_table()
        assert expected == actual

        assert position.type() == PositionType.FOUR_THREE
