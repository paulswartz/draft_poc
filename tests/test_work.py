from datetime import date, timedelta
from draft_poc.work import (
    parse,
    CrewSchedule,
    Duty,
    DutyActivity,
    Piece,
    ScheduleType,
    Trip,
    TripType,
    Vacation,
    VacationType,
)
from draft_poc.ids import DutyAssignment, DutyInternalId
from draft_poc.roster import DayOfWeek


class TestCrewSchedule:
    def test_simple_parse(self):
        row = "55751|abg47011|Sunday|02|BUS42017|Charlestown Sunday"
        expected = CrewSchedule(
            internal_id=55751,
            name="abg47011",
            schedule_type=ScheduleType.SUNDAY,
            scenario_id=2,
            booking_id="BUS42017",
            description="Charlestown Sunday",
        )
        actual = parse("crew_schedule_YYYYMMDD_HHMMSS.txt", row)
        assert expected == actual


class TestDuty:
    def test_simple_parse(self):
        row = "2252|748729|55751|1245|fto2|480|4|474|0|0|0|0|0"
        expected = Duty(
            id=DutyAssignment("2252"),
            internal_id=DutyInternalId(748729),
            schedule_internal_id=55751,
            operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
            type="fto2",
            regular=timedelta(hours=8),
            overtime=timedelta(minutes=4),
            spread=timedelta(minutes=474),
            travel=timedelta(minutes=0),
            swing_allowance=timedelta(minutes=0),
            guarantee=timedelta(minutes=0),
            spread_allowance_half=timedelta(minutes=0),
            spread_allowance_straight=timedelta(minutes=0),
        )
        actual = parse("duty_YYYYMMDD_HHMMSS.txt", row)
        assert actual == expected


class TestPiece:
    def test_simple_parse(self):
        row = "4655751|0505|305|0835|515|4804474|12345|748729|12345"
        expected = Piece(
            internal_id=4655751,
            report=timedelta(hours=5, minutes=5),
            clear=timedelta(hours=8, minutes=35),
            block_id=4804474,
            block_operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.WEDNESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
            duty_id=748729,
            duty_operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.WEDNESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
        )
        actual = parse("piece_YYYYMMDD_HHMMSS.txt", row)
        assert expected == actual


class TestDutyActivity:
    def test_simple_parse(self):
        row = "8282570|12345|Sign-on|413|253|423|263"
        expected = DutyActivity(
            internal_id=8282570,
            operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.WEDNESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
            type="Sign-on",
            start=timedelta(hours=4, minutes=13),
            end=timedelta(hours=4, minutes=23),
        )
        actual = parse("duty_activity_YYYYMMDD_HHMMSS.txt", row)
        assert expected == actual


class TestTrip:
    def test_simple_parse(self):
        row = "36684644|1245|Pull-in|0|2114|1274|2132|1292|fhill|soham|3549048|1245"
        expected = Trip(
            internal_id=36684644,
            operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
            type=TripType.PULL_IN,
            pilot=False,
            start_time=timedelta(hours=21, minutes=14),
            end_time=timedelta(hours=21, minutes=32),
            start_place="fhill",
            end_place="soham",
            block_id=3549048,
            block_operating_days=[
                DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY,
            ],
        )
        actual = parse("trip_YYYYMMDD_HHMMSS.txt", row)
        assert expected == actual


class TestVacation:
    def test_simple_parse(self):
        row = "00001943|10/08/2017|10/14/2017|Weekly"
        expected = Vacation(
            employee_id="00001943",
            start_date=date(2017, 10, 8),
            end_date=date(2017, 10, 14),
            type=VacationType.WEEKLY,
        )
        actual = parse("vacation_YYYYMMDD_HHMMSS.txt", row)
        assert expected == actual
