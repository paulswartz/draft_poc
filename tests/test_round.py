from datetime import date, datetime
from zoneinfo import ZoneInfo
from draft_poc.round import parse, BidRound, BidGroup, BidEmployee, BidType


class TestBidRound:
    def test_simple_parse(self):
        row = "R|BUS22021-122|Work|02/09/2021|03/03/2021|Work|1||122|Arborway|BUS22021|03/14/2021|06/19/2021"
        expected = BidRound(
            process_id="BUS22021-122",
            round_id="Work",
            opening_date=date(2021, 2, 9),
            closing_date=date(2021, 3, 3),
            type=BidType.WORK,
            rank=1,
            service_context=None,
            division_id="122",
            division_description="Arborway",
            booking_id="BUS22021",
            bid_period_start=date(2021, 3, 14),
            bid_period_end=date(2021, 6, 19),
        )
        actual = parse(row)
        assert actual == expected


class TestBidGroup:
    def test_simple_parse(self):
        row = "G|BUS22021-122|Work|1|02/11/2021|500p"
        expected = BidGroup(
            process_id="BUS22021-122",
            round_id="Work",
            group_rank=1,
            cutoff_datetime=datetime(
                2021, 2, 11, 17, tzinfo=ZoneInfo("America/New_York")
            ),
        )
        actual = parse(row)
        assert actual == expected


class TestBidEmployee:
    def test_simple_parse(self):
        row = "E|BUS22021-122|Work|3|8|01234|Doe Jane|000100"
        expected = BidEmployee(
            process_id="BUS22021-122",
            round_id="Work",
            group_rank=3,
            employee_rank=8,
            employee_id="01234",
            employee_name="Doe Jane",
            job_class="000100",
        )
        actual = parse(row)
        assert actual == expected
