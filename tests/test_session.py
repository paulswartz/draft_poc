from datetime import date
from draft_poc.ids import (
    ProcessId,
    RoundId,
    SessionId,
    SchedulingUnit,
    DivisionId,
    BookingId,
    RosterSetId,
    RosterSetInternalId,
    RosterId,
)
from draft_poc.session import parse, BidSession, RosterSet, RosterAvailability
from draft_poc.round import BidType


class TestBidSession:
    def test_simple_parse(self):
        row = "S|BUS22021-122|Work|Work|Work||Rsc122|122|BUS22021|03/14/2021|06/19/2021"
        expected = BidSession(
            process_id=ProcessId("BUS22021-122"),
            round_id=RoundId("Work"),
            session_id=SessionId("Work"),
            type=BidType.WORK,
            service_context=None,
            scheduling_unit=SchedulingUnit("Rsc122"),
            division_id=DivisionId("122"),
            booking_id=BookingId("BUS22021"),
            bid_period_start=date(2021, 3, 14),
            bid_period_end=date(2021, 6, 19),
        )
        actual = parse(row)
        assert expected == actual


class TestRosterSet:
    def test_simple_parse(self):
        row = "R|BUS22021|Work_PT|Rsc122|Rsc122|546296506|1|Base"
        expected = RosterSet(
            booking_id=BookingId("BUS22021"),
            session_id=SessionId("Work_PT"),
            scheduling_unit=SchedulingUnit("Rsc122"),
            roster_set_id=RosterSetId("Rsc122"),
            roster_set_internal_id=RosterSetInternalId(546296506),
            scenario=1,
            service_context="Base",
        )
        actual = parse(row)
        assert expected == actual


class TestRosterAvailability:
    def test_simple_parse(self):
        row = "A|BUS22021|Work|Rsc125|2042346781|4-BTT|1"
        expected = RosterAvailability(
            booking_id=BookingId("BUS22021"),
            session_id=SessionId("Work"),
            roster_set_id=RosterSetId("Rsc125"),
            roster_set_internal_id=RosterSetInternalId(2042346781),
            roster_id=RosterId("4-BTT"),
            available=True,
        )
        actual = parse(row)
        assert expected == actual
