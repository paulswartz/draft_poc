"""
Type representing all of the parsed data for a single Pick.
"""
# pylint: disable=too-few-public-methods
from collections import defaultdict
from datetime import date, timedelta
from typing import Any, Iterable, Optional
import attr
from draft_poc.ids import (
    DutyInternalId,
    EmployeeId,
    RosterId,
    RosterPositionInternalId,
    BookingId,
    DivisionId,
    SchedulingUnit,
)
from draft_poc.repo import Repo
from draft_poc.round import (
    BidRound,
    BidGroup,
    BidEmployee,
    RoundKey,
    GroupKey,
)
from draft_poc.session import BidSession, RosterAvailability, RosterSet, RosterSetKey
from draft_poc.roster import (
    DayOfWeek,
    Assignment,
    CrewAssignment,
    NonCrewAssignment,
    RosterPosition,
    RosterDay,
)
from draft_poc.work import Duty, Piece, Trip


def timedelta_as_timestr(delta: timedelta) -> str:
    """
    Render a timedelta as a time, with times >24 using "x" as the last character.
    """
    seconds = delta.total_seconds()
    (hours, seconds) = divmod(seconds, 3600)
    (minutes, _seconds) = divmod(seconds, 60)
    if hours == 0:
        tag = "a"
        hours = 12
    elif hours < 12:
        tag = "a"
    elif hours == 12:
        tag = "p"
    elif hours < 24:
        tag = "p"
        hours -= 12
    elif hours == 24:
        tag = "x"
        hours -= 12
    else:
        tag = "x"
        hours -= 24

    return f"{int(hours)}{int(minutes):02d}{tag}"


def timedelta_as_durationstr(delta: timedelta) -> str:
    """
    Render a timedelta as an hour/minute duration
    """
    seconds = delta.total_seconds()
    (hours, seconds) = divmod(seconds, 3600)
    (minutes, _seconds) = divmod(seconds, 60)

    return f"{int(hours)}h{int(minutes):02d}"


@attr.s(auto_attribs=True)
class BidSessionAssignment:
    """
    An assignment of a roster position to an employee during a round.
    """

    employee_id: EmployeeId
    booking_id: BookingId
    start_date: date
    end_date: date
    division_id: DivisionId
    scheduling_unit: Optional[SchedulingUnit]
    roster_id: RosterId
    roster_position_internal_id: RosterPositionInternalId
    record_type: str = attr.ib(default="empbook")

    def to_parts(self) -> list[str]:
        """
        Convert this assignment to a list of parts.
        """
        return [
            self.record_type,
            str(self.employee_id),
            str(self.booking_id),
            self.start_date.strftime("%0m/%0d/%Y"),
            self.end_date.strftime("%0m/%0d/%Y"),
            self.division_id,
            self.scheduling_unit or "",
            str(self.roster_id),
            str(self.roster_position_internal_id),
        ]


@attr.s(auto_attribs=True)
class Pick:
    """
    Time period when employees select their work for the upcoming Rating.
    """

    booking_id: str
    start_date: date
    end_date: date
    repo: Repo = attr.ib(repr=False)
    assignments: list[BidSessionAssignment] = attr.ib(repr=False, factory=list)

    def rounds(self) -> list[BidRound]:
        """
        Return the list of BidRounds.
        """
        return list(self.repo.all(BidRound))

    def sessions(self, round_key: RoundKey) -> Iterable[BidSession]:
        """
        Return the BidSessions for a given round.
        """
        return self.repo.query(BidSession, round_key)

    def groups(self, round_key: RoundKey) -> list[BidGroup]:
        """
        Given a BidRoundId, return the groups for that round.
        """
        return list(self.repo.query(BidGroup, round_key))

    def employees_in_group(self, group_key: GroupKey) -> Iterable[BidEmployee]:
        """
        Given a BidGroupId, return the employees in that group.
        """
        return self.repo.query(BidEmployee, group_key)

    def roster_sets(self, roster_set_key: RosterSetKey) -> Iterable[RosterSet]:
        """
        Return the available RosterSets for a given RosterSetId.

        Checks the RosterAvailability before yielding each RosterSet.
        """
        return self.repo.query(RosterSet, roster_set_key)

    def assign_work(
        self,
        employee: BidEmployee,
        rnd: BidRound,
        session: BidSession,
        position: RosterPosition,
    ) -> None:
        """
        Assign the given RosterPosition to the BidEmployee.
        """
        bid_assignment = BidSessionAssignment(
            employee_id=employee.employee_id,
            booking_id=session.booking_id,
            start_date=rnd.bid_period_start,
            end_date=rnd.bid_period_end,
            division_id=session.division_id,
            scheduling_unit=session.scheduling_unit,
            roster_id=position.roster_id,
            roster_position_internal_id=position.roster_position_internal_id,
        )
        self.assignments.append(bid_assignment)

    def roster_positions(self, roster_set: RosterSet) -> list[RosterPosition]:
        """
        Return the list of available roster positions in a given set.
        """
        available_rosters = [
            availability.roster_key()
            for availability in self.repo.query(
                RosterAvailability, roster_set.availability_key()
            )
            if availability.available
        ]
        roster_days = defaultdict(list)
        for roster_id in available_rosters:
            for day in self.repo.query(RosterDay, roster_id):
                roster_days[day.position_key()].append(day)
        positions = [RosterPosition.from_days(days) for days in roster_days.values()]
        positions.sort(key=self.roster_position_key)
        return positions

    def roster_position_key(self, position: RosterPosition) -> Any:
        """
        Return a key used for sorting the roster position.

        The rules for sorting a position are:
        1) postition type (see `draft_poc.roster.PositionType`)
        2) 2 consecutive days off
          - Saturday and Sunday
          - Sunday and Monday
          - Friday and Saturday
          - Thursday and Friday
          - Monday and Tuesday
          - Wednesday and Thursday
          - Tuesday and Wednesday
        3) earliest off time for the first piece
        4) highest pay (currently using total work time)
        5) lowest roster number
        """
        days_off_order = {
            frozenset([DayOfWeek.SATURDAY, DayOfWeek.SUNDAY]): 0,
            frozenset([DayOfWeek.MONDAY, DayOfWeek.SUNDAY]): 1,
            frozenset([DayOfWeek.SATURDAY, DayOfWeek.FRIDAY]): 2,
            frozenset([DayOfWeek.THURSDAY, DayOfWeek.FRIDAY]): 3,
            frozenset([DayOfWeek.MONDAY, DayOfWeek.TUESDAY]): 4,
            frozenset([DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY]): 5,
            frozenset([DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY]): 6,
        }
        days_off_default = len(days_off_order)
        days_off_index: int = days_off_order.get(position.days_off(), days_off_default)
        roster_number = position.roster_position_id
        if position.type().is_crew():
            crew_assignment = next(
                roster_day.assignment
                for roster_day in position.days
                if isinstance(roster_day.assignment, CrewAssignment)
            )
            duty = self.repo.query(Duty, crew_assignment.duty_internal_id)[0]
            first_piece = self.repo.query(Piece, crew_assignment.duty_internal_id)[0]
            return (
                position.type(),
                days_off_index,
                first_piece.clear,
                -duty.total_paid_time(),  # sort higher values first
                roster_number,
            )

        return (position.type(), days_off_index, roster_number)

    def duty(self, duty_internal_id: DutyInternalId) -> Duty:
        """
        Return a Duty given the internal ID, or KeyError if not found.
        """
        duties = self.repo.query(Duty, duty_internal_id)
        if len(duties) == 1:
            return duties[0]
        raise KeyError(duty_internal_id)

    def pretty_assignment(self, assignment: Assignment) -> str:
        """
        Render the given assignment as a nice string.

        For crew assignment, render as:

        1001 7h52
        lynn lynn
        432p 142x

        For non-crew assignments, render as their value.
        """
        if assignment == NonCrewAssignment.LIST_8HR:
            return "LR08   8h00"
        if assignment == NonCrewAssignment.LIST_10HR:
            return "LR10  10h00"
        if isinstance(assignment, NonCrewAssignment):
            return assignment.value

        duty = self.repo.query(Duty, assignment.duty_internal_id)[0]
        pieces = self.repo.query(Piece, assignment.duty_internal_id)
        trips = [
            trip for piece in pieces for trip in self.repo.query(Trip, piece.block_id)
        ]
        start_time = pieces[0].report
        start_place = trips[0].start_place
        end_time = pieces[-1].clear
        end_place = trips[-1].end_place
        return (
            f"{assignment.assignment:5} {timedelta_as_durationstr(duty.total_paid_time()):>6}\n"
            f"{start_place:5} {end_place:>6}\n"
            f"{timedelta_as_timestr(start_time):5} {timedelta_as_timestr(end_time):>6}"
        )

    @classmethod
    def from_repo(cls, repo: Repo) -> "Pick":
        """
        Create a Pick given a Repo of the records.
        """
        bid_round: BidRound = next(iter(repo.all(BidRound)))

        return cls(
            repo=repo,
            booking_id=bid_round.booking_id,
            start_date=bid_round.bid_period_start,
            end_date=bid_round.bid_period_end,
        )
