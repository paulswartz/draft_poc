"""
Type for a RosterDay.
"""
# pylint: disable=too-few-public-methods
from datetime import date
import enum
from typing import cast, Union
import attr
from draft_poc.ids import (
    DutyAssignment,
    DutyInternalId,
    BookingId,
    RosterSetId,
    RosterSetInternalId,
    RosterId,
    RosterPositionId,
    RosterPositionInternalId,
)
from draft_poc.round import us_date
from draft_poc.session import RosterKey


class DayOfWeek(enum.IntEnum):
    """
    Wrapper around parsing the days of the week.
    """

    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    @classmethod
    def from_str(cls, string: Union[str, "DayOfWeek"]) -> "DayOfWeek":
        """
        Parse a string like "Monday" or "Saturday" into a DayOfWeek.
        """
        if isinstance(string, cls):
            return string

        return cls(
            [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday",
            ].index(cast(str, string))
        )


DayDate = Union[DayOfWeek, date]


def day_date(string: Union[str, DayDate]) -> DayDate:
    """
    Parse a string into a DayDate.
    """
    if isinstance(string, (date, DayOfWeek)):
        return string

    try:
        return DayOfWeek.from_str(string)
    except ValueError:
        return us_date(string)


@attr.s(auto_attribs=True, frozen=True)
class CrewAssignment:
    """
    An assignment of work for a given day.
    """

    assignment: DutyAssignment
    duty_internal_id: DutyInternalId = attr.ib(converter=int)

    @staticmethod
    def is_list() -> bool:
        """
        Matching the NonCrewAssignment API.
        """
        return False

    @staticmethod
    def is_vacation_relief() -> bool:
        """
        Matching the NonCrewAssignment API.
        """
        return False


class NonCrewAssignment(enum.Enum):
    """
    Various types of non-crew assignments.
    """

    OFF = "OFF"
    OFF_LIST = "OL"
    OFF_LIST_PART_TIME = "OLP"
    LIST_8HR = "LR08"
    LIST_10HR = "LR10"
    LIST_PART_TIME = "LRP"
    VACATION_RELIEF = "VR"
    VACATION_RELIEF_PART_TIME = "VRP"
    UNKNOWN = ""

    def is_list(self) -> bool:
        """
        Return True if this is a List assignment.
        """
        cls = self.__class__
        return self in {cls.LIST_8HR, cls.LIST_10HR, cls.LIST_PART_TIME}

    def is_vacation_relief(self) -> bool:
        """
        Return True if this is a Vacation Relief assignment.
        """
        cls = self.__class__
        return self in {cls.VACATION_RELIEF, cls.VACATION_RELIEF_PART_TIME}


Assignment = Union[NonCrewAssignment, CrewAssignment]


class PositionType(enum.IntEnum):
    """
    Type of RosterPosition, used for sorting.

    The sort order of positions types is:

    - 5/2 work
    - 4/3 work
    - vacation relief
    - list work
    """

    FIVE_TWO = 0
    FOUR_THREE = 1
    VACATION_RELIEF = 2
    LIST = 3
    UNKNOWN = 4

    def is_crew(self) -> bool:
        """
        Returns whether this type of position has a crew schedule.
        """
        return self in {self.FIVE_TWO, self.FOUR_THREE}

    def pretty(self) -> str:
        """
        Return a pretty representation of this type.
        """
        return {
            self.FIVE_TWO: "5/2",
            self.FOUR_THREE: "4/3",
            self.VACATION_RELIEF: "VR",
            self.LIST: "List",
            self.UNKNOWN: "Unknown",
        }[self]


def assignment(duty_assignment: str, duty_internal_id: str) -> Assignment:
    """
    Parse a string into an Assignment.
    """
    if duty_internal_id != "":
        return CrewAssignment(
            DutyAssignment(duty_assignment), DutyInternalId(int(duty_internal_id))
        )

    return NonCrewAssignment(duty_assignment)


RosterPositionKey = tuple[RosterKey, RosterPositionId]


@attr.s(auto_attribs=True)
class RosterDay:
    """
    A Roster for a given day of the week or date.
    """

    # pylint: disable=too-many-instance-attributes
    booking_id: BookingId = attr.ib(converter=BookingId)
    roster_set_id: RosterSetId = attr.ib(converter=RosterSetId)
    roster_set_internal_id: RosterSetInternalId = attr.ib(converter=int)
    roster_id: RosterId = attr.ib(converter=RosterId)
    roster_position_id: RosterPositionId = attr.ib(converter=RosterPositionId)
    roster_position_internal_id: RosterPositionInternalId = attr.ib(converter=int)
    day: DayDate = attr.ib(converter=day_date)
    assignment: Assignment
    crew_schedule_id: int = attr.ib(converter=int)

    def key(self) -> RosterKey:
        """
        Returns the RosterId of which this day is a part.
        """
        return (self.roster_set_internal_id, self.roster_id)

    def position_key(self) -> RosterPositionKey:
        """
        Return the RosterPositionId of which this day is a part.
        """
        return (self.key(), self.roster_position_id)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "RosterDay":
        """
        Create a RosterDay from an already-split row.
        """
        [
            booking_id,
            roster_set_id,
            roster_set_internal_id,
            roster_id,
            roster_position_id,
            roster_position_internal_id,
            day,
            duty_assignment,
            duty_internal_id,
            crew_schedule_id,
        ] = parts

        return cls(
            booking_id,
            roster_set_id,
            roster_set_internal_id,
            roster_id,
            roster_position_id,
            roster_position_internal_id,
            day,
            assignment(duty_assignment, duty_internal_id),
            crew_schedule_id,
        )


@attr.s(auto_attribs=True)
class RosterPosition:
    """
    A group of work for one Employee.
    """

    booking_id: BookingId
    roster_set_id: RosterSetId
    roster_set_internal_id: RosterSetInternalId
    roster_id: RosterId
    roster_position_id: RosterPositionId
    roster_position_internal_id: RosterPositionInternalId
    days: list[RosterDay] = attr.ib(repr=False)

    def key(self) -> RosterPositionKey:
        """
        Key representing all of the days for this RosterPosition.
        """

    def type(self) -> PositionType:
        """
        Return the type of position this RosterPosition represents.
        """
        work_for_days_of_week = [
            roster_day.assignment for roster_day in self.base_schedule()
        ]
        if any(
            assignment for assignment in work_for_days_of_week if assignment.is_list()
        ):
            return PositionType.LIST
        if any(
            assignment
            for assignment in work_for_days_of_week
            if assignment.is_vacation_relief()
        ):
            return PositionType.VACATION_RELIEF

        days_off = len(self.days_off())

        if days_off == 2:
            return PositionType.FIVE_TWO
        if days_off == 3:
            return PositionType.FOUR_THREE

        return PositionType.UNKNOWN

    def base_schedule(self) -> list[RosterDay]:
        """
        Returns the base schedule RosterDays: those assigned to a day of the week.
        """
        return [
            roster_day
            for roster_day in self.days
            if isinstance(roster_day.day, DayOfWeek)
        ]

    def days_off(self) -> frozenset[DayOfWeek]:
        """
        Return the DayOfWeek which are not worked.
        """
        return frozenset(
            roster_day.day
            for roster_day in self.days
            if isinstance(roster_day.day, DayOfWeek)
            and roster_day.assignment == NonCrewAssignment.OFF
        )

    def day_table(self) -> list[tuple[DayDate, Assignment]]:
        """
        Returns a list of assignments for this position.

        Strips off any days where the service isn't different from the
        typical work on that day of the week.

        The sorting is:
        - Sunday through Saturday
        - any extra dates
        """
        base: list[tuple[DayDate, Assignment]] = [
            (day.day, day.assignment) for day in self.base_schedule()
        ]
        base.sort()
        # extra days which are different from the normal assignment
        extras: list[tuple[DayDate, Assignment]] = [
            (day.day, day.assignment)
            for day in self.days
            if isinstance(day.day, date)
            and day.assignment != base[day.day.weekday()][1]
        ]

        base.sort(
            key=lambda x: (cast(DayOfWeek, x[0]).value + 1) % 7
        )  # put Sunday first
        extras.sort()

        return base + extras

    @classmethod
    def from_days(cls, roster_days: list[RosterDay]) -> "RosterPosition":
        """
        Group a list of RosterDays into a Roster.

        Assumes that all the provided RosterDays are part of the same Roster.
        """
        day = roster_days[0]
        return cls(
            booking_id=day.booking_id,
            roster_set_id=day.roster_set_id,
            roster_set_internal_id=day.roster_set_internal_id,
            roster_id=day.roster_id,
            roster_position_id=day.roster_position_id,
            roster_position_internal_id=day.roster_position_internal_id,
            days=roster_days,
        )


def parse(row: str) -> RosterDay:
    """
    Convert a CSV-ish row into a RosterDay.
    """
    parts = row.split("|")
    return RosterDay.from_parts(parts)
