"""
Types for working with Bid Rounds/Groups/Employees.
"""
# pylint: disable=too-few-public-methods
from datetime import datetime, date, time
import enum
from typing import Any, Optional, Union
from zoneinfo import ZoneInfo
import attr
from draft_poc.ids import RoundId, ProcessId, DivisionId, BookingId, EmployeeId

US_EASTERN = ZoneInfo("America/New_York")


def us_date(string: Union[str, date]) -> date:
    """
    Convert a MM/DD/YYYY date into a `datetime.date`.

    If it's already a date, pass it through.
    """
    if isinstance(string, date):
        return string

    return datetime.strptime(string, "%m/%d/%Y").date()


def short_time(string: str) -> time:
    """
    Convert a HHMMp time into a `datetime.time`.
    """
    return datetime.strptime(string + "m", "%I%M%p").time()


def optional_string(string: str) -> Optional[str]:
    """
    Return None if the string is empty, otherwise pass it through.
    """
    if string in ["", None]:
        return None

    return str(string)


class BidType(enum.Enum):
    """
    Represents what is being picked in a given BidRound.
    """

    WORK = "Work"
    VACATION = "Vacation"


RoundKey = tuple[ProcessId, RoundId]
GroupKey = tuple[RoundKey, int]


@attr.s(auto_attribs=True)
class BidRound:
    """
    Defines which, when, and in what order employees will select preferences,
    """

    # pylint: disable=too-many-instance-attributes
    process_id: ProcessId = attr.ib(converter=ProcessId)
    round_id: RoundId = attr.ib(converter=RoundId)
    opening_date: date = attr.ib(converter=us_date)
    closing_date: date = attr.ib(converter=us_date)
    type: BidType = attr.ib(converter=BidType)
    rank: int = attr.ib(converter=int)
    service_context: Optional[str] = attr.ib(converter=optional_string)
    division_id: DivisionId = attr.ib(converter=DivisionId)
    division_description: str
    booking_id: BookingId = attr.ib(converter=BookingId)
    bid_period_start: date = attr.ib(converter=us_date)
    bid_period_end: date = attr.ib(converter=us_date)

    def key(self) -> RoundKey:
        """
        Return a grouping key for this round.
        """
        return (self.process_id, self.round_id)

    def sort_key(self) -> Any:
        """
        Key for sorting rounds.
        """
        return (self.rank, self.opening_date)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "BidRound":
        """
        Create a BidRound from an already-split row.
        """
        return cls(*parts)


@attr.s(auto_attribs=True)
class BidGroup:
    """
    Defines which employees are picking at which time, and how long they have
    to select preferences.
    """

    process_id: ProcessId = attr.ib(converter=ProcessId)
    round_id: RoundId = attr.ib(converter=RoundId)
    group_rank: int = attr.ib(converter=int)
    cutoff_datetime: datetime

    def key(self) -> RoundKey:
        """
        Return the BidRoundId for the BidRound of which this group is a part.
        """
        return (self.process_id, self.round_id)

    def group_key(self) -> GroupKey:
        """
        Return the BidGroupId for this group.
        """
        return (self.key(), self.group_rank)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "BidGroup":
        """
        Create a BidGroup from an already-split row.
        """
        [process_id, round_id, group_rank, cutoff_date, cutoff_time] = parts
        return cls(
            process_id,
            round_id,
            group_rank,
            datetime.combine(
                us_date(cutoff_date), short_time(cutoff_time), tzinfo=US_EASTERN
            ),
        )


@attr.s(auto_attribs=True)
class BidEmployee:
    """
    Employee picking as a part of a BidGroup.
    """

    process_id: ProcessId = attr.ib(converter=ProcessId)
    round_id: RoundId = attr.ib(converter=RoundId)
    group_rank: int = attr.ib(converter=int)
    employee_rank: int = attr.ib(converter=int)
    employee_id: EmployeeId = attr.ib(converter=EmployeeId)
    employee_name: str
    job_class: str

    def round_key(self) -> RoundKey:
        """
        Return the RoundKey of which this employee is a part.
        """
        return (self.process_id, self.round_id)

    def key(self) -> GroupKey:
        """
        Return the GroupKey of which this employee is a part.
        """
        return (self.round_key(), self.group_rank)

    def sort_key(self) -> Any:
        """
        Return a key (usable for sorting) for this employee.
        """
        return (self.key(), self.employee_rank)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "BidEmployee":
        """
        Create a BidEmployee from an already-split row.
        """
        return cls(*parts)


def parse(row: str) -> Union[BidRound, BidGroup, BidEmployee]:
    """
    Parse a CSV-ish row into a BidRound/BidGroup/BidEmployee.
    """
    [tag, *rest] = row.split("|")

    if tag == "R":
        return BidRound.from_parts(rest)

    if tag == "G":
        return BidGroup.from_parts(rest)

    if tag == "E":
        return BidEmployee.from_parts(rest)

    raise ValueError(f"unexpected tag: {repr(tag)}")
