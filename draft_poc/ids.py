"""
General types of IDs used in multiple files.
"""
from typing import NewType

BookingId = NewType("BookingId", str)  # BUS22021
ProcessId = NewType("ProcessId", str)  # BUS22021-122
RosterSetId = NewType("RosterSetId", str)  # Rsc122
RosterSetInternalId = NewType("RosterSetInternalId", int)  # 12345678
RosterId = NewType("RosterId", str)  # 1-BB
RosterPositionId = NewType("RosterPositionId", str)  # 122001
RosterPositionInternalId = NewType("RosterPositionInternalId", int)  # 3
DutyAssignment = NewType("DutyAssignment", str)  # 1001
DutyInternalId = NewType("DutyInternalId", int)  # 123456
DivisionId = NewType("DivisionId", str)  # 122
RoundId = NewType("RoundId", str)  # Work
EmployeeId = NewType("EmployeeId", str)  # 00123
SessionId = NewType("SessionId", str)  # Work
SchedulingUnit = NewType("SchedulingUnit", str)  # Rsc122
