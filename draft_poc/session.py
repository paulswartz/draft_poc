"""
Types for working with Bid Sessions / Rosters / Roster Sets.
"""
# pylint: disable=too-few-public-methods
from datetime import date
from typing import cast, Optional, Union
import attr
from draft_poc.ids import (
    ProcessId,
    RoundId,
    SessionId,
    SchedulingUnit,
    DivisionId,
    BookingId,
    RosterSetId,
    RosterSetInternalId,
    RosterId,
)
from draft_poc.round import BidType, optional_string, us_date

SessionKey = tuple[ProcessId, RoundId]
RosterSetKey = tuple[BookingId, Optional[SchedulingUnit], SessionId]
RosterAvailabilityKey = tuple[RosterSetInternalId, SessionId]
RosterKey = tuple[RosterSetInternalId, RosterId]


def optional_scheduling_unit(string: Optional[str]) -> Optional[SchedulingUnit]:
    """
    Convert a string to an SchedulingUnit.

    If the string is empty, return None.
    """
    if string in {"", None}:
        return None
    return SchedulingUnit(cast(str, string))


def bool_from_string(string: Union[str, bool]) -> bool:
    """
    Returns True if given the string "1" or a True boolean.
    """
    return string in [True, "1"]


@attr.s(auto_attribs=True)
class BidSession:
    """
    Wrapper around all Bid Groups.
    """

    # pylint: disable=too-many-instance-attributes
    process_id: ProcessId = attr.ib(converter=ProcessId)
    round_id: RoundId = attr.ib(converter=RoundId)
    session_id: SessionId = attr.ib(converter=SessionId)
    type: BidType = attr.ib(converter=BidType)
    service_context: Optional[str] = attr.ib(converter=optional_string)
    scheduling_unit: Optional[SchedulingUnit] = attr.ib(
        converter=optional_scheduling_unit
    )
    division_id: DivisionId = attr.ib(converter=DivisionId)
    booking_id: BookingId = attr.ib(converter=BookingId)
    bid_period_start: date = attr.ib(converter=us_date)
    bid_period_end: date = attr.ib(converter=us_date)

    def key(self) -> SessionKey:
        """
        Return a key for looking up the BidSession given a round.
        """
        return (self.process_id, self.round_id)

    def roster_set_key(self) -> RosterSetKey:
        """
        Return a key used to look up the RosterSet used by this session.
        """
        return (self.booking_id, self.scheduling_unit, self.session_id)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "BidSession":
        """
        Create a BidSession from an already-split row.
        """
        return cls(*parts)


@attr.s(auto_attribs=True)
class RosterSet:
    """
    A group of roster positions.
    """

    booking_id: BookingId = attr.ib(converter=BookingId)
    session_id: SessionId = attr.ib(converter=SessionId)
    scheduling_unit: Optional[SchedulingUnit] = attr.ib(
        converter=optional_scheduling_unit
    )
    roster_set_id: RosterSetId = attr.ib(converter=RosterSetId)
    roster_set_internal_id: RosterSetInternalId = attr.ib(converter=int)
    scenario: int = attr.ib(converter=int)
    service_context: str

    def key(self) -> RosterSetKey:
        """
        Return a key for grouping the RosterSets.
        """
        return (self.booking_id, self.scheduling_unit, self.session_id)

    def availability_key(self) -> RosterAvailabilityKey:
        """
        Return the key for this RosterSet's availability records.
        """
        return (self.roster_set_internal_id, self.session_id)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "RosterSet":
        """
        Create a Roster set from an already-split row.
        """
        return cls(*parts)


@attr.s(auto_attribs=True)
class RosterAvailability:
    """
    Whether a given roster is available to be picked as a part of the BidSession.
    """

    booking_id: BookingId = attr.ib(converter=BookingId)
    session_id: SessionId = attr.ib(converter=SessionId)
    roster_set_id: RosterSetId = attr.ib(converter=RosterSetId)
    roster_set_internal_id: RosterSetInternalId = attr.ib(converter=int)
    roster_id: RosterId = attr.ib(converter=RosterId)
    available: bool = attr.ib(converter=bool_from_string)

    def key(self) -> RosterAvailabilityKey:
        """
        Return a key for looking up the RosterAvailability.
        """
        return (self.roster_set_internal_id, self.session_id)

    def roster_key(self) -> RosterKey:
        """
        Return the key for the rosters this availability represents.
        """
        return (self.roster_set_internal_id, self.roster_id)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "RosterAvailability":
        """
        Create a RosterAvailability from an already-split row.
        """
        return cls(*parts)


def parse(row: str) -> Union[BidSession, RosterSet, RosterAvailability]:
    """
    Parse a CSV-ish row in a BidSession/RosterSet/RosterAvailability.
    """
    [tag, *parts] = row.split("|")
    if tag == "S":
        return BidSession.from_parts(parts)
    if tag == "R":
        return RosterSet.from_parts(parts)
    if tag == "A":
        return RosterAvailability.from_parts(parts)

    raise ValueError(f"unexpected tag {repr(tag)}")
