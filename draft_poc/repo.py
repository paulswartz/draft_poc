"""
Stores the parsed records and provides an interface for querying.
"""
from collections import defaultdict
from collections.abc import Iterable, Iterator, Mapping, Sequence
from typing import Any, Protocol, TypeVar
import attr


class Keyed(Protocol):  # pylint: disable=too-few-public-methods
    """
    Simple protocol for objects which have a key.
    """

    def key(self) -> Any:
        """
        The key for this record, used for lookups later.
        """


T = TypeVar("T")  # pylint: disable=invalid-name


@attr.s(auto_attribs=True, repr=False)
class Repo:
    """
    Wrapper around the parsed records, and simple methods for querying.
    """

    data: Mapping[type, Mapping[Any, list[Any]]] = attr.ib(
        factory=lambda: defaultdict(lambda: defaultdict(list))
    )

    def all(self, typ: type[T]) -> Iterator[T]:
        """
        Returns an iterable of all records of a given key.
        """
        for records in self.data.get(typ, {}).values():
            yield from records

    def keys(self, typ: type) -> Iterator[Any]:
        """
        Returns the valid keys for the given type.
        """
        return iter(self.data.get(typ, {}).keys())

    def query(self, typ: type[T], key: Any) -> Sequence[T]:
        """
        Returns the records with the given key.
        """
        return self.data.get(typ, {}).get(key, [])

    @classmethod
    def from_records(cls, records: Iterable[Keyed]) -> "Repo":
        """
        Populate a repo from a list of keyed records.
        """
        repo = cls()
        for record in records:
            typ = type(record)
            key = record.key()
            repo.data[typ][key].append(record)
        for (typ, mapping) in repo.data.items():
            if hasattr(typ, "sort_key"):
                sort_key = typ.sort_key  # type: ignore[attr-defined]
            else:
                sort_key = typ.key

            for typ_records in mapping.values():
                typ_records.sort(key=sort_key)
        return repo
