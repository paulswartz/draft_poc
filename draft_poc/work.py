"""
Types for HASTUS Work data.
"""
# pylint: disable=too-few-public-methods
from datetime import date, timedelta
import enum
from typing import Union
import attr
from draft_poc.ids import DutyAssignment, DutyInternalId
from draft_poc.round import us_date
from draft_poc.roster import DayOfWeek
from draft_poc.session import bool_from_string


class ScheduleType(enum.Enum):
    """
    The days when the crew schedule operates.
    """

    WEEKDAY = "Weekday"
    SATURDAY = "Saturday"
    SUNDAY = "Sunday"


class VacationType(enum.Enum):
    """
    Type of vacation: weekly or dated.
    """

    WEEKLY = "Weekly"
    DATED = "Dated"


class TripType(enum.Enum):
    """
    Types of trip parts.
    """

    PULL_OUT = "Pull-out"
    DEADHEAD = "Deadhead"
    REGULAR = "Regular"
    PULL_IN = "Pull-in"
    OPPORTUNITY = "Opportunity"


def days_of_week(string: Union[str, list[DayOfWeek]]) -> list[DayOfWeek]:
    """
    Convert a list of digits into a list of DayOfWeek.

    These digits are 1-indexed, so Monday is 1 and Sunday is 7.
    """
    if isinstance(string, list):
        return string

    return [DayOfWeek(int(digit) - 1) for digit in string]


def timedelta_string(string: Union[str, timedelta]) -> timedelta:
    """
    Convert a number of minutes as a string to a `timedelta`.
    """
    if isinstance(string, timedelta):
        return string

    return timedelta(minutes=int(string))


@attr.s(auto_attribs=True)
class CrewSchedule:
    """
    One record for each day and holiday in the RosterSet.
    """

    internal_id: int = attr.ib(converter=int)
    name: str
    schedule_type: ScheduleType = attr.ib(converter=ScheduleType)
    scenario_id: int = attr.ib(converter=int)
    booking_id: str
    description: str

    def key(self) -> int:
        """
        Key for indexing the schedule.
        """
        return self.internal_id

    @classmethod
    def from_parts(cls, parts: list[str]) -> "CrewSchedule":
        """
        Create a CrewSchedule from an already-split row.
        """
        return cls(*parts)


@attr.s(auto_attribs=True)
class Duty:
    """
    One record for each duty of the crew schedules only.
    """

    # pylint: disable=too-many-instance-attributes disable=invalid-name
    id: DutyAssignment = attr.ib(converter=DutyAssignment)
    internal_id: DutyInternalId = attr.ib(converter=int)
    schedule_internal_id: int = attr.ib(converter=int)
    operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)
    type: str
    regular: timedelta = attr.ib(converter=timedelta_string)
    overtime: timedelta = attr.ib(converter=timedelta_string)
    spread: timedelta = attr.ib(converter=timedelta_string)
    travel: timedelta = attr.ib(converter=timedelta_string)
    swing_allowance: timedelta = attr.ib(converter=timedelta_string)
    guarantee: timedelta = attr.ib(converter=timedelta_string)
    spread_allowance_half: timedelta = attr.ib(converter=timedelta_string)
    spread_allowance_straight: timedelta = attr.ib(converter=timedelta_string)

    def key(self) -> DutyInternalId:
        """
        Key for indexing the duty.
        """
        return self.internal_id

    def total_paid_time(self) -> timedelta:
        """
        Return the total paid time.
        """
        return self.regular + self.overtime + self.travel

    @classmethod
    def from_parts(cls, parts: list[str]) -> "Duty":
        """
        Create a Duty from an already-split row.
        """
        return cls(*parts)


@attr.s(auto_attribs=True)
class Piece:
    """
    One record for each piece of the crew schedules only.
    """

    internal_id: int = attr.ib(converter=int)
    report: timedelta = attr.ib(converter=timedelta_string)
    clear: timedelta = attr.ib(converter=timedelta_string)
    block_id: int = attr.ib(converter=int)
    block_operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)
    duty_id: int = attr.ib(converter=int)
    duty_operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)

    def key(self) -> int:
        """
        Key for indexing the piece.
        """
        return self.duty_id

    def sort_key(self) -> timedelta:
        """
        Sort pieces by their clear time.
        """
        return self.clear

    @classmethod
    def from_parts(cls, parts: list[str]) -> "Piece":
        """
        Create a Piece from an already-split row.
        """
        [
            internal_id,
            _,
            report,
            _,
            clear,
            block_id,
            block_operating_days,
            duty_id,
            duty_operating_days,
        ] = parts
        return cls(
            internal_id=internal_id,
            report=report,
            clear=clear,
            block_id=block_id,
            block_operating_days=block_operating_days,
            duty_id=duty_id,
            duty_operating_days=duty_operating_days,
        )


@attr.s(auto_attribs=True)
class DutyActivity:
    """
    One record for each duty activity of each duty of the crew schedules only.
    """

    internal_id: int = attr.ib(converter=int)
    operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)
    type: str
    start: timedelta = attr.ib(converter=timedelta_string)
    end: timedelta = attr.ib(converter=timedelta_string)

    def key(self) -> int:
        """
        Key for indexing the activity.
        """
        return self.internal_id

    @classmethod
    def from_parts(cls, parts: list[str]) -> "DutyActivity":
        """
        Create a DutyActivity from an already-split row.
        """
        [internal_id, operating_days, work_type, _, start, _, end] = parts
        return cls(
            internal_id=internal_id,
            operating_days=operating_days,
            type=work_type,
            start=start,
            end=end,
        )


@attr.s(auto_attribs=True)
class Trip:
    """
    One record for each trip of the crew schedules only.
    """

    # pylint: disable=too-many-instance-attributes
    internal_id: int = attr.ib(converter=int)
    operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)
    type: TripType = attr.ib(converter=TripType)
    pilot: bool = attr.ib(converter=bool_from_string)
    start_time: timedelta = attr.ib(converter=timedelta_string)
    end_time: timedelta = attr.ib(converter=timedelta_string)
    start_place: str
    end_place: str
    block_id: int = attr.ib(converter=int)
    block_operating_days: list[DayOfWeek] = attr.ib(converter=days_of_week)

    @classmethod
    def from_parts(cls, parts: list[str]) -> "Trip":
        """
        Create a Trip from an already-split row.
        """
        [
            internal_id,
            operating_days,
            trip_type,
            pilot,
            _,
            start_time,
            _,
            end_time,
            start_place,
            end_place,
            block_id,
            block_operating_days,
        ] = parts
        return cls(
            internal_id=internal_id,
            operating_days=operating_days,
            type=trip_type,
            pilot=pilot,
            start_time=start_time,
            end_time=end_time,
            start_place=start_place,
            end_place=end_place,
            block_id=block_id,
            block_operating_days=block_operating_days,
        )

    def key(self) -> int:
        """
        Return the block ID for easier lookups.
        """
        return self.block_id


@attr.s(auto_attribs=True)
class Vacation:
    """
    One record for each week/day of vacation for each employee.
    """

    employee_id: str
    start_date: date = attr.ib(converter=us_date)
    end_date: date = attr.ib(converter=us_date)
    type: VacationType = attr.ib(converter=VacationType)

    def key(self) -> str:
        """
        Key for indexing the vacation.
        """
        return self.employee_id

    @classmethod
    def from_parts(cls, parts: list[str]) -> "Vacation":
        """
        Create a Vacation from an already-split row.
        """
        return cls(*parts)


def parse(
    filename: str, row: str
) -> Union[CrewSchedule, Duty, DutyActivity, Piece, Trip, Vacation]:
    """
    Parse a row from a given filename into a record.

    Since the records themselves don't indicate what kind of data are there,
    we use the filename to disambiguate.
    """
    parts = row.split("|")
    if filename.startswith("crew_schedule"):
        return CrewSchedule.from_parts(parts)

    if filename.startswith("duty_activity"):
        return DutyActivity.from_parts(parts)

    if filename.startswith("duty"):
        return Duty.from_parts(parts)

    if filename.startswith("piece"):
        return Piece.from_parts(parts)

    if filename.startswith("trip"):
        return Trip.from_parts(parts)

    if filename.startswith("vacation"):
        return Vacation.from_parts(parts)

    raise ValueError(f"unable to parse {repr(row)} from {str(filename)}")
