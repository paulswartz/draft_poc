"""
Main entrypoint for the Draft POC.
"""
# pylint: disable=too-few-public-methods
from datetime import date, timedelta
from pathlib import Path
from typing import Any, Callable, Iterable, Iterator
from tabulate import tabulate
from draft_poc.repo import Repo
from draft_poc.pick import Pick, timedelta_as_durationstr
from draft_poc import session, roster, work
import draft_poc.round


Parser = Callable[[str], Any]


DATA_DIR = Path("data")
PARSERS: list[tuple[str, Parser]] = [
    ("*Bid_Round*.csv", draft_poc.round.parse),
    ("*Bid_Session*.csv", session.parse),
    ("*Roster_Day*.csv", roster.parse),
    # ("crew_schedule_*", lambda x: work.parse("crew_schedule", x)),
    # ("duty_activity_*", lambda x: work.parse("duty_activity", x)),
    ("duty_2*", lambda x: work.parse("duty", x)),
    ("piece_*", lambda x: work.parse("piece", x)),
    ("trip_*", lambda x: work.parse("trip", x)),
    # ("vacation_*", lambda x: work.parse("vacation", x)),
]


def gather_data() -> Iterable[Any]:
    """
    Gather all the records from our data directory.
    """
    seen = set()
    for (glob, parser) in PARSERS:
        for filepath in DATA_DIR.glob(glob):
            if filepath in seen:
                continue
            seen.add(filepath)
            with filepath.open() as record_file:
                for line in record_file:
                    try:
                        yield parser(line.rstrip())
                    except Exception as exc:
                        raise ValueError(
                            f"unable to parse {repr(line)} from {str(filepath)} with {repr(parser)}"
                        ) from exc


def position_display(i: Any) -> str:
    """
    Simple function to render the roster position data.
    """
    if isinstance(i, draft_poc.roster.DayOfWeek):
        return ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"][i.value]
    if isinstance(i, date):
        return i.strftime("%m/%d")
    if hasattr(i, "assignment"):
        # CrewAssignment
        return str(i.assignment)
    if hasattr(i, "value"):
        # other Enum
        return str(i.value)
    return str(i)


def round_sessions(
    pick: Pick, round_key: draft_poc.round.RoundKey
) -> list[tuple[session.BidSession, Iterator[roster.RosterPosition]]]:
    """
    Returns the sessions and their available positions.
    """
    return [
        (session, session_positions(pick, session))
        for session in pick.sessions(round_key)
    ]


def session_positions(
    pick: Pick, bid_session: session.BidSession
) -> Iterator[roster.RosterPosition]:
    """
    Return the available positions for a given BidSession.
    """
    for roster_set in pick.roster_sets(bid_session.roster_set_key()):
        yield from pick.roster_positions(roster_set)


def calculate_totals(pick: Pick, position: roster.RosterPosition) -> list[str]:
    """
    Calculate the work, travel, guaranteed, and overtime work values.
    """
    work_duration: timedelta = timedelta()
    travel_duration: timedelta = timedelta()
    guaranteed_duration: timedelta = timedelta()
    overtime_duration: timedelta = timedelta()
    for roster_day in position.base_schedule():
        assignment = roster_day.assignment
        if assignment == roster.NonCrewAssignment.LIST_8HR:
            work_duration += timedelta(hours=8)
        elif assignment == roster.NonCrewAssignment.LIST_10HR:
            work_duration += timedelta(hours=10)
        elif isinstance(assignment, roster.CrewAssignment):
            duty = pick.duty(assignment.duty_internal_id)
            work_duration += duty.regular + duty.overtime
            travel_duration += duty.travel
            guaranteed_duration += duty.guarantee
            overtime_duration += duty.overtime
    return [
        timedelta_as_durationstr(delta)
        for delta in [
            work_duration,
            travel_duration,
            guaranteed_duration,
            overtime_duration,
        ]
    ]


def print_position(pick: Pick, position: roster.RosterPosition) -> None:
    """
    Print the information for a given RosterPosition.
    """
    (days, results) = zip(*position.day_table())
    days_formatted = [position_display(i) for i in days]
    results_formatted = [pick.pretty_assignment(i) for i in results]
    for line in tabulate(
        [days_formatted, results_formatted], headers="firstrow", tablefmt="plain"
    ).split("\n"):
        print("", line, sep="\t")
    print("")


def print_totals(pick: Pick, position: roster.RosterPosition) -> None:
    """
    Print total times (work/travel/guaranteed/overtime.
    """
    headers = ["Work", "Travel", "Guar", "OT Wk"]
    values = calculate_totals(pick, position)
    for line in tabulate([headers, values], headers="firstrow", tablefmt="plain").split(
        "\n"
    ):
        print("", line, sep="\t")
    print("")


def main() -> None:
    """
    Entrypoint for the "go" CLI.
    """
    repo = Repo.from_records(gather_data())
    pick = Pick.from_repo(repo)
    print(f"Pick {pick.booking_id} for rating {pick.start_date} - {pick.end_date}\n")
    print(f"{len(pick.rounds())} bid rounds:")
    for (i, rnd) in enumerate(pick.rounds(), 1):
        print(f"Round {i}, {rnd.type.name.title()} for {rnd.division_description}")
        print(
            f"Opens {rnd.opening_date.strftime('%x')}, closes {rnd.closing_date.strftime('%x')}."
        )
        this_round_sessions = round_sessions(pick, rnd.key())
        for group in pick.groups(rnd.key()):
            print(
                f"Group {group.group_rank} cuts off at {group.cutoff_datetime.strftime('%c')}"
            )
            for employee in pick.employees_in_group(group.group_key()):
                print(
                    f"{employee.employee_rank}\t{employee.employee_id} - {employee.employee_name}"
                )
                for (bid_session, positions) in this_round_sessions:
                    try:
                        position = next(positions)
                    except StopIteration:
                        print("\t Unable to assign work - no remaining positions")
                        continue
                    pick.assign_work(employee, rnd, bid_session, position)
                    print(
                        f"\t{bid_session.session_id} -> "
                        f"position {position.roster_position_id} ({position.type().pretty()})"
                    )
                    print_position(pick, position)
                    print_totals(pick, position)
            print("")
        print("")

    print(f"Made {len(pick.assignments)} assignments.")

    with open("empbook.csv", "w") as empbook:
        for assignment in pick.assignments:
            print(*assignment.to_parts(), sep="|", file=empbook)


if __name__ == "__main__":
    main()
